package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Juegos extends AppCompatActivity implements View.OnClickListener{

    Button btnJuego1, btnJuego2, btnJuego3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juegos);
        referenciar();
    }

    private void referenciar() {
        btnJuego1 = findViewById(R.id.btnJuego1);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnJuego1.setOnClickListener(this);
        btnJuego2  = findViewById(R.id.btnJuego2);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnJuego2.setOnClickListener(this);
        btnJuego3  = findViewById(R.id.btnJuego3);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnJuego3.setOnClickListener(this);
    }
    //este metodo me escucha cada vez que le den click
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnJuego1:
                Toast.makeText(this, "Ha seleccionado Juego1", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent1 = new Intent(this, Juego1.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent1);
                break;
            case R.id.btnJuego2:
                Toast.makeText(this, "Ha seleccionado Juego2", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent2 = new Intent(this, Juego2.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent2);
                break;
            case R.id.btnJuego3:
                Toast.makeText(this, "Ha seleccionado Juego3", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent3 = new Intent(this, Juego3.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent3);
                break;

        }
    }

}