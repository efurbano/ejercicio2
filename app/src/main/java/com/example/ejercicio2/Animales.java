package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Animales extends AppCompatActivity  implements View.OnClickListener {

    Button btnDomesticos, btnSalvajes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animales);
        referenciar();
    }

    private void referenciar() {
        btnDomesticos = findViewById(R.id.btnDomesticos);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnDomesticos.setOnClickListener(this);
        btnSalvajes  = findViewById(R.id.btnSalvajes);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnSalvajes.setOnClickListener(this);

    }

    @Override
    //este metodo me escucha cada vez que le den click
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnDomesticos:
                Toast.makeText(this, "Ha seleccionado animalews domesticos", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent1 = new Intent(this,Domesticos.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent1);
                break;
            case R.id.btnSalvajes:
                Toast.makeText(this, "Ha seleccionado animales salvajes", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent2 = new Intent(this,Salvajes.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent2);
                break;

        }
    }
}