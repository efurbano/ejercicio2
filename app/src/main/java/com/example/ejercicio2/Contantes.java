package com.example.ejercicio2;

public class Contantes {
    public static final String NOM_BD="Ciudades";
    public static final int VERSION_BD=1;

    public static final String NOM_TABLA= "Ciudad";
    public static final String COLUM1="Nombre";
    public static final String COLUM2="Codigo";

    public static final String tablaCiudad =
            "create table "
            +NOM_TABLA+ " ("+COLUM1+ " TEXT, "+ COLUM2
            +" INTEGER)";

}
