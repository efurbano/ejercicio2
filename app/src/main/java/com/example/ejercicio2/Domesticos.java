package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class Domesticos extends AppCompatActivity implements View.OnClickListener{

    ImageButton imgCaballo,imgGallina,imgPerro,imgGato;
    TextView txtCaballo,txtGallina, txtPerro, txtGato;
    MediaPlayer caballo,gallina,gato,perro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_domesticos);
        referenciar();
    }

    private void referenciar() {
        imgCaballo = findViewById(R.id.imgCaballo);
        imgCaballo.setOnClickListener(this);
        imgGallina = findViewById(R.id.imgGallina);
        imgGallina.setOnClickListener(this);
        imgPerro   = findViewById(R.id.imgPerro);
        imgPerro.setOnClickListener(this);
        imgGato    = findViewById(R.id.imgGato);
        imgGato.setOnClickListener(this);
        txtCaballo = findViewById(R.id.txtCaballo);
        txtGallina = findViewById(R.id.txtGallina);
        txtPerro   = findViewById(R.id.txtPerro);
        txtGato   = findViewById(R.id.txtGato);
        //almacena sonido en la variable gallina
        caballo    = MediaPlayer.create(this,R.raw.caballo);
        gallina    = MediaPlayer.create(this,R.raw.gallina);
        gato       = MediaPlayer.create(this,R.raw.gato);
        perro      = MediaPlayer.create(this,R.raw.perro);
    }

    @Override
    //este metodo me escucha cada vez que le den click
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgCaballo:
                txtCaballo.setText("Wara");
                caballo.start();
                break;
            case R.id.imgGallina:
                txtGallina.setText("Atwall");
                gallina.start();
                break;
            case R.id.imgPerro:
                txtPerro.setText("Wera");
                perro.start();
                break;
            case R.id.imgGato:
                txtGato.setText("Mish");
                gato.start();
                break;
        }
    }
}