package com.example.ejercicio2;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class ManagerDB {
    //almacenamos la conexion
    Dbhelper dbhelper;
    SQLiteDatabase db;
    Context context;

    public ManagerDB(Context context) {
        //hacemos la conexion de la BD
        dbhelper = new Dbhelper(context);

        //this.context = context;
    }
    public void openDbWrite(){ //metodo para abrir en modo escritura
        db = dbhelper.getWritableDatabase();
    }
    public void openDbRead(){ //metodo para abrir en modo lectura
        db = dbhelper.getReadableDatabase();
    }
    public void closeDb(){ //metodo para cerrar la BD
        if (db!=null) { //verificamos si hay conexion
            db.close();
        }
    }
    public void insertData(Ciudad ciudad){

        //1ro abrir la base de datos en  modo escritura
        openDbWrite();
        //2do creamos el contenedor de valores
        ContentValues values = new ContentValues();
        //Ciudad ciudad = new Ciudad(); // llamamos nuestro pojo
        //llenamos nuestra tabla
        values.put(Contantes.COLUM1,ciudad.getNombre());
        values.put(Contantes.COLUM2,ciudad.getCodigo());
        //3ro insertamos el contenedor de valores a la tabla
        db.insert(Contantes.NOM_TABLA,null,values);
        //4to cerramos la base de datos
        closeDb();
    }


}
