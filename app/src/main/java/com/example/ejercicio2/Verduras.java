package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class Verduras extends AppCompatActivity  implements View.OnClickListener{

    ImageButton imgCebolla,imgZanahoria,imgTomate,imgPimiento;
    TextView txtCebolla,txtZanahoria, txtTomate, txtPimiento, txtVerdura;
    MediaPlayer cebolla,zanahoria,tomate,pimiento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verduras);
        referenciar();
    }

    private void referenciar() {

        imgCebolla = findViewById(R.id.imgCebolla);
        imgCebolla.setOnClickListener(this);
        imgZanahoria = findViewById(R.id.imgZanahoria);
        imgZanahoria.setOnClickListener(this);
        imgTomate   = findViewById(R.id.imgTomate);
        imgTomate.setOnClickListener(this);
        imgPimiento    = findViewById(R.id.imgPimiento);
        imgPimiento.setOnClickListener(this);
        txtVerdura   = findViewById(R.id.txtVerdura);
        txtCebolla   = findViewById(R.id.txtCebolla);
        txtZanahoria = findViewById(R.id.txtZanahoria);
        txtTomate   = findViewById(R.id.txtTomate);
        txtPimiento = findViewById(R.id.txtPimiento);
        //almacena sonido en la variable gallina
        cebolla    = MediaPlayer.create(this,R.raw.caballo);
        zanahoria  = MediaPlayer.create(this,R.raw.gallina);
        tomate       = MediaPlayer.create(this,R.raw.gato);
        pimiento      = MediaPlayer.create(this,R.raw.perro);
    }

    @Override
    //este metodo me escucha cada vez que le den click
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgCebolla:
                txtCebolla.setText("Wara");
                cebolla.start();
                break;
            case R.id.imgZanahoria:
                txtZanahoria.setText("Atwall");
                zanahoria.start();
                break;
            case R.id.imgTomate:
                txtTomate.setText("Wera");
                tomate.start();
                break;
            case R.id.imgPimiento:
                txtPimiento.setText("Mish");
                pimiento.start();
                break;
        }
    }
}