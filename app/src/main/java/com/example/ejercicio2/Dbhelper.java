package com.example.ejercicio2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Dbhelper extends SQLiteOpenHelper {
    public Dbhelper(@Nullable Context context ) {
        super(context, Contantes.NOM_BD, null, Contantes.VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
            //creamos la base datos
        db.execSQL(Contantes.tablaCiudad);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
