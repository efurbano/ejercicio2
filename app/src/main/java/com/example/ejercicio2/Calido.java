package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class Calido extends AppCompatActivity  implements View.OnClickListener{

    ImageButton imgPapaGuata,imgArracacha,imgMaiz,imgOca;
    TextView txtPapaGuata,txtArracacha, txtMaiz, txtOca, txtCalido;
    MediaPlayer papaguata,arracacha,maiz,oca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calido);
        referenciar();
    }

    private void referenciar() {
        imgPapaGuata = findViewById(R.id.imgPapaGuata);
        imgPapaGuata.setOnClickListener(this);
        imgArracacha = findViewById(R.id.imgArracacha);
        imgArracacha.setOnClickListener(this);
        imgMaiz   = findViewById(R.id.imgMaiz);
        imgMaiz.setOnClickListener(this);
        imgOca    = findViewById(R.id.imgOca);
        imgOca.setOnClickListener(this);
        txtCalido   = findViewById(R.id.txtCalido);
        txtPapaGuata   = findViewById(R.id.txtPapaGuata);
        txtArracacha = findViewById(R.id.txtArracacha);
        txtMaiz   = findViewById(R.id.txtMaiz);
        txtOca = findViewById(R.id.txtOca);
        //almacena sonido en la variable gallina
        papaguata    = MediaPlayer.create(this,R.raw.caballo);
        arracacha  = MediaPlayer.create(this,R.raw.gallina);
        maiz       = MediaPlayer.create(this,R.raw.gato);
        oca      = MediaPlayer.create(this,R.raw.perro);
    }

    @Override
    //este metodo me escucha cada vez que le den click
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgPapaGuata:
                txtPapaGuata.setText("Wara");
                papaguata.start();
                break;
            case R.id.imgArracacha:
                txtArracacha.setText("Atwall");
                arracacha.start();
                break;
            case R.id.imgMaiz:
                txtMaiz.setText("Wera");
                maiz.start();
                break;
            case R.id.imgOca:
                txtOca.setText("Mish");
                oca.start();
                break;
        }
    }
}