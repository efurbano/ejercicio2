package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class Frio extends AppCompatActivity implements View.OnClickListener{

    ImageButton imgUlluco,imgPapaColorada,imgPapaAmarilla,imgRepollo;
    TextView txtUlluco,txtPapaColorada, txtPapaAmarilla, txtRepollo, txtFrio;
    MediaPlayer ulluco,papacolorada,papaamarilla,repollo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frio);
        referenciar();
    }

    private void referenciar() {
        imgUlluco = findViewById(R.id.imgUlluco);
        imgUlluco.setOnClickListener(this);
        imgPapaColorada = findViewById(R.id.imgPapaColorada);
        imgPapaColorada.setOnClickListener(this);
        imgPapaAmarilla   = findViewById(R.id.imgPapaAmarilla);
        imgPapaAmarilla.setOnClickListener(this);
        imgRepollo    = findViewById(R.id.imgRepollo);
        imgRepollo.setOnClickListener(this);
        txtFrio   = findViewById(R.id.txtFrio);
        txtUlluco   = findViewById(R.id.txtUlluco);
        txtPapaColorada = findViewById(R.id.txtPapaColorada);
        txtPapaAmarilla   = findViewById(R.id.txtPapaAmarilla);
        txtRepollo = findViewById(R.id.txtRepollo);
        //almacena sonido en la variable gallina
        ulluco    = MediaPlayer.create(this,R.raw.caballo);
        papacolorada  = MediaPlayer.create(this,R.raw.gallina);
        papaamarilla       = MediaPlayer.create(this,R.raw.gato);
        repollo      = MediaPlayer.create(this,R.raw.perro);
    }

    @Override
    //este metodo me escucha cada vez que le den click
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgUlluco:
                txtUlluco.setText("Wara");
                ulluco.start();
                break;
            case R.id.imgPapaColorada:
                txtPapaColorada.setText("Atwall");
                papacolorada.start();
                break;
            case R.id.imgPapaAmarilla:
                txtPapaAmarilla.setText("Wera");
                papaamarilla.start();
                break;
            case R.id.imgRepollo:
                txtRepollo.setText("Mish");
                repollo.start();
                break;
        }
    }
}