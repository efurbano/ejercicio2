package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class Salvajes extends AppCompatActivity implements View.OnClickListener{

    ImageButton imgAguila,imgArmadillo,imgOso,imgSerpiente;
    TextView txtAguila,txtArmadillo, txtOso, txtSerpiente;
    MediaPlayer aguila,armadillo,oso,serpiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salvajes);
        referenciar();
    }
    private void referenciar() {
        imgAguila = findViewById(R.id.imgAguila);
        imgAguila.setOnClickListener(this);
        imgArmadillo = findViewById(R.id.imgArmadillo);
        imgArmadillo.setOnClickListener(this);
        imgOso   = findViewById(R.id.imgOso);
        imgOso.setOnClickListener(this);
        imgSerpiente    = findViewById(R.id.imgSerpiente);
        imgSerpiente.setOnClickListener(this);
        txtAguila = findViewById(R.id.txtAguila);
        txtArmadillo = findViewById(R.id.txtArmadillo);
        txtOso   = findViewById(R.id.txtOso);
        txtSerpiente   = findViewById(R.id.txtSerpiente);
        //almacena sonido en la variable gallina
        aguila    = MediaPlayer.create(this,R.raw.aguila);
        armadillo    = MediaPlayer.create(this,R.raw.armadillo);
        oso       = MediaPlayer.create(this,R.raw.oso);
        serpiente      = MediaPlayer.create(this,R.raw.serpiente);
    }

    @Override
    //este metodo me escucha cada vez que le den click
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgAguila:
                txtAguila.setText("Washe");
                aguila.start();
                break;
            case R.id.imgArmadillo:
                txtArmadillo.setText("chule");
                armadillo.start();
                break;
            case R.id.imgOso:
                txtOso.setText("Wijj");
                oso.start();
                break;
            case R.id.imgSerpiente:
                txtSerpiente.setText("Wul");
                serpiente.start();
                break;
        }
    }

}