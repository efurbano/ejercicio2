package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnAnimales,  btnHuerta, btnSitios, btnJuegos,btnIngresar2;
    //ManagerDB managerDB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        referenciar();
        //llamarBd();
    }

    //private void llamarBd() {
        /*Dbhelper dbhelper = new Dbhelper(MainActivity.this);//instancia la conexion de la base de datos
        SQLiteDatabase db=dbhelper.getWritableDatabase(); //coloco la base de datos en modo escritura
        Toast.makeText(this,"Base de datos creada",
                Toast.LENGTH_SHORT).show();*/
      /*  managerDB = new ManagerDB(MainActivity.this);
        managerDB.insertData();
        Toast.makeText(this, "SISISI ha insertado correctamante",
                    Toast.LENGTH_SHORT).show();*/
    //}

    private void referenciar() {
        btnAnimales = findViewById(R.id.btnAnimales);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnAnimales.setOnClickListener(this);
        btnHuerta   = findViewById(R.id.btnHuerta);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnHuerta.setOnClickListener(this);
        btnSitios   = findViewById(R.id.btnSitios);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnSitios.setOnClickListener(this);
        btnJuegos   = findViewById(R.id.btnJuegos);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnJuegos.setOnClickListener(this);
        btnIngresar2   = findViewById(R.id.btnIngresar2);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnIngresar2.setOnClickListener(this);
    }

    @Override
    //este metodo me escucha cada vez que le den click
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAnimales:
                Toast.makeText(MainActivity.this,"Ha seleccionado Animales",Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent1 = new Intent(MainActivity.this,Animales.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent1);
                break;

            case R.id.btnHuerta:
                Toast.makeText(MainActivity.this,"Ha seleccionado Huerta",Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent2 = new Intent(MainActivity.this,Huerta.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent2);
                break;


            case R.id.btnSitios:
                Toast.makeText(MainActivity.this, "Ha seleccionado Sitios sagrados", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent4 = new Intent(MainActivity.this,Sitios.class);
                startActivity(intent4);
                break;
            case R.id.btnJuegos:
                Toast.makeText(MainActivity.this, "Ha seleccionado Juegos...", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent5 = new Intent(MainActivity.this,Juegos.class);
                startActivity(intent5);
                break;
            case R.id.btnIngresar2:
                Toast.makeText(MainActivity.this, "Ha seleccionado Ingresar ciudades...", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent6 = new Intent(MainActivity.this,InsertarCiudad.class);
                startActivity(intent6);
                break;
        }
    }
}