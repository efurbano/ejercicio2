package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Sitios extends AppCompatActivity implements View.OnClickListener {

    Button btnMontanas, btnLagunas, btnSagrados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sitios);
        referenciar();
    }
    private void referenciar() {
        btnMontanas = findViewById(R.id.btnMontanas);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnMontanas.setOnClickListener(this);
        btnLagunas  = findViewById(R.id.btnLagunas);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnLagunas.setOnClickListener(this);
        btnSagrados  = findViewById(R.id.btnSagrados);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnSagrados.setOnClickListener(this);

    }
    //este metodo me escucha cada vez que le den click
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnMontanas:
                Toast.makeText(this, "Ha seleccionado Montañas", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent1 = new Intent(this,Montanas.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent1);
                break;
            case R.id.btnLagunas:
                Toast.makeText(this, "Ha seleccionado Lagunas", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent2 = new Intent(this,Lagunas.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent2);
                break;
            case R.id.btnSagrados:
                Toast.makeText(this, "Ha seleccionado sitios sagrados", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent3 = new Intent(this,Sagrados.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent3);
                break;

        }
    }


}