package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Trueque extends AppCompatActivity implements View.OnClickListener {

    Button btnCaliente, btnFrio, btnNumero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trueque);
        referenciar();
    }

    private void referenciar() {
        btnCaliente = findViewById(R.id.btnCaliente);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnCaliente.setOnClickListener(this);

        btnFrio  = findViewById(R.id.btnFrio);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnFrio.setOnClickListener(this);

        btnNumero = findViewById(R.id.btnNumero);
        btnNumero.setOnClickListener(this);

    }
    @Override
    //este metodo me escucha cada vez que le den click
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnCaliente:
                Toast.makeText(this, "Ha seleccionado alimentos de tierra caliente", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent1 = new Intent(Trueque.this,Calido.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent1);
                break;
            case R.id.btnFrio:
                Toast.makeText(this, "Ha seleccionado alimentos de tierra fria", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent2 = new Intent(Trueque.this,Frio.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent2);
                break;
            case R.id.btnNumero:
                Toast.makeText(this, "Ha seleccionado numeros", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent4 = new Intent(Trueque.this, Numero.class);
                //inicializamos el objeto de tipo Intent para que vaya al layaout de la clase
                startActivity(intent4);
                break;

        }
    }
}