package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Huerta extends AppCompatActivity implements  View.OnClickListener{

    Button btnHerramientas, btnFrutas, btnVerduras, btnTrueque;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_huerta);
        referenciar();
    }
    private void referenciar() {
        btnHerramientas = findViewById(R.id.btnHerramientas);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnHerramientas.setOnClickListener(this);
        btnFrutas   = findViewById(R.id.btnFrutas);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnFrutas.setOnClickListener(this);
        btnVerduras   = findViewById(R.id.btnVerduras);
        //aqui tenemos que decirle que está en esta interfa el click llega arriba y no sabe a donda va
        btnVerduras.setOnClickListener(this);
        btnTrueque   = findViewById(R.id.btnTrueque);
        btnTrueque.setOnClickListener(this);

    }
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnVerduras:
                Toast.makeText(Huerta.this, "Ha seleccionado verduras...", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent1 = new Intent(Huerta.this,Verduras.class);
                startActivity(intent1);
                break;

            case R.id.btnTrueque:
                Toast.makeText(Huerta.this, "Ha seleccionado Ingresar huerta...", Toast.LENGTH_SHORT).show();
                //al metodo Intent le enviamos dos parametros la clase donde estamos y la clase a donde queremos ir
                Intent intent2 = new Intent(Huerta.this,Trueque.class);
                startActivity(intent2);
                break;
        }
    }


}