package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class InsertarCiudad extends AppCompatActivity {
    EditText etxtCiudad,etxtCodigo;
    Button btnInsertar;
    ManagerDB managerDB;
    String nombre;
    int codigo;
    Ciudad ciudad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertar_ciudad);
        referenciar();
    }
    private void llamarBd() {
        /*Dbhelper dbhelper = new Dbhelper(MainActivity.this);//instancia la conexion de la base de datos
        SQLiteDatabase db=dbhelper.getWritableDatabase(); //coloco la base de datos en modo escritura
        Toast.makeText(this,"Base de datos creada",
                Toast.LENGTH_SHORT).show();*/

        managerDB = new ManagerDB(InsertarCiudad.this);
        managerDB.insertData(ciudad);
        Toast.makeText(InsertarCiudad.this, "SISISIS se creo registro",
                Toast.LENGTH_SHORT).show();
    }

    private void referenciar() {
        etxtCiudad = findViewById(R.id.etxtCiudad);
        etxtCodigo = findViewById(R.id.etxtCodigo);
        btnInsertar = findViewById(R.id.btnInsertar);
        btnInsertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                nombre = etxtCiudad.getText().toString();
                codigo = Integer.parseInt(etxtCodigo.getText().toString());
                // enviamos las variable nombre y codigo a pojo
                ciudad = new Ciudad(nombre, codigo);
                llamarBd();
            }
        });
    }
}