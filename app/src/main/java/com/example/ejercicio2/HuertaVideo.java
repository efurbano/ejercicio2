package com.example.ejercicio2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class HuertaVideo extends AppCompatActivity {

    VideoView video;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_huertavideo);

        video =findViewById(R.id.vvHuerta); //referencia para la variable
        video.setVideoPath("android.resource://" + getPackageName()+"/"+R.raw.mihuerta);
        MediaController mediaController = new MediaController(this); //activar controles de pausa y star
        mediaController.setAnchorView(video);  //ancho del video
        video.setMediaController(mediaController); //asigna controles al video view
        video.start(); //ejecuta el video


    }
}